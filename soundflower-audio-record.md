## How To Record Internal Audio with QuickTime Player Screen Recording


1. System Preferences > Sound > Output > Screen Record OUT UE
2. System Preferences > Sound > Input > MacBook Pro Microphone > Input Volume: to zero
3. Screen Record (QuickTime) > Options > Screen Record IN


References:

[yt1](https://www.youtube.com/watch?v=iYDSOHYf8II)

[yt2](https://www.youtube.com/watch?v=dNYZOaf3Gvs)

[github_download](https://github.com/mattingalls/Soundflower/releases/)
