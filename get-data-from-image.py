from PIL import Image
import pytesseract
import sys

# Load the image

image_path = sys.argv[1]
image = Image.open(image_path)

# Use OCR to extract text from the image
text = pytesseract.image_to_string(image)

# Display the extracted text
#image.show()
print(text)
