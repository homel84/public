select c.thread#, c.sequence# as receiving, a.sequence# as applying, c.sequence#-a.sequence# as gap, a.block# , a.status
from gv$managed_standby a join gv$managed_standby c on (c.thread#=a.thread# and c.client_process='LGWR' and a.process='MRP0');
