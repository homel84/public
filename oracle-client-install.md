### Oracle Instant Client Installation

List installed packages
```sh
rpm -qa | grep sql
yum list installed | grep sql
```

Yum available packages for oracle instant client
```sh
[root@localhost yum.repos.d] yum list oracle-instantclient*
Available Packages
oracle-instantclient18.3-basic.x86_64                                                                                    18.3.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.3-basiclite.x86_64                                                                                18.3.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.3-devel.x86_64                                                                                    18.3.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.3-jdbc.x86_64                                                                                     18.3.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.3-odbc.x86_64                                                                                     18.3.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.3-sqlplus.x86_64                                                                                  18.3.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.3-tools.x86_64                                                                                    18.3.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.5-basic.x86_64                                                                                    18.5.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.5-basiclite.x86_64                                                                                18.5.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.5-devel.x86_64                                                                                    18.5.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.5-jdbc.x86_64                                                                                     18.5.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.5-odbc.x86_64                                                                                     18.5.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.5-sqlplus.x86_64                                                                                  18.5.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient18.5-tools.x86_64                                                                                    18.5.0.0.0-3                                                                                 ol7_oracle_instantclient
oracle-instantclient19.3-basic.x86_64                                                                                    19.3.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.3-basiclite.x86_64                                                                                19.3.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.3-devel.x86_64                                                                                    19.3.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.3-jdbc.x86_64                                                                                     19.3.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.3-odbc.x86_64                                                                                     19.3.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.3-sqlplus.x86_64                                                                                  19.3.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.3-tools.x86_64                                                                                    19.3.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.5-basic.x86_64                                                                                    19.5.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.5-basiclite.x86_64                                                                                19.5.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.5-devel.x86_64                                                                                    19.5.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.5-jdbc.x86_64                                                                                     19.5.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.5-odbc.x86_64                                                                                     19.5.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.5-sqlplus.x86_64                                                                                  19.5.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.5-tools.x86_64                                                                                    19.5.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.6-basic.x86_64                                                                                    19.6.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.6-basiclite.x86_64                                                                                19.6.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.6-devel.x86_64                                                                                    19.6.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.6-jdbc.x86_64                                                                                     19.6.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.6-odbc.x86_64                                                                                     19.6.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.6-sqlplus.x86_64                                                                                  19.6.0.0.0-1                                                                                 ol7_oracle_instantclient
oracle-instantclient19.6-tools.x86_64                                                                                    19.6.0.0.0-1                                                                                 ol7_oracle_instantclient
```

Subscription-manager disable (optional)
```sh
vim /etc/yum/pluginconf.d/subscription-manager.conf

enabled=0

yum makecache
```

Install p7zip (if zip/unzip not available)
```sh
yum install p7zip
```

Download client
```sh
sh wget.sh
7za x V46097-01.zip
```

Install client
```sh
rpm -ilv oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm oracle-instantclient12.1-sqlplus-12.1.0.2.0-1.x86_64.rpm
rpm -qa | grep instantclient
cd /usr/lib/oracle/12.1/client64/
ls -ltrh
```

Update bash_profile
```sh
su - <user>
vi .bash_profile
export LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib
export TNS_ADMIN=/usr/lib/oracle/12.1/client64
export ORACLE_HOME=/usr/lib/oracle/12.1/client64

PATH=$PATH:$HOME/bin:$ORACLE_HOME/bin
```

Uninstalling client
```sh
rpm -e oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm oracle-instantclient12.1-sqlplus-12.1.0.2.0-1.x86_64.rpm

```